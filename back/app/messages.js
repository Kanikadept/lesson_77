const path = require('path');
const express = require('express');
const multer = require('multer');
const fileDb = require('../fs');
const {nanoid} = require('nanoid');
const config = require('../config');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
})

const upload = multer({storage});

const router = express.Router();


router.get('/', (req, res) => {

    if(req.query.datetime) {

        const result = new Date(req.query.datetime);
        if(result instanceof Date && !isNaN(result)) {
            const lastMessages = fileDb.getItemsByTime(result);
            return res.send(lastMessages);
        } else {
            return res.send({error: 'Invalid Date'});
        }
    }

    const messages = fileDb.getItems();
    res.send(messages);
});

router.post('/', upload.single('image'), (req, res) => {

    const message = req.body;

    if(req.file) {
        message.image = req.file.filename;
    }

    if (message.message.length === 0) {
        const error = {"error": "Author and message must be present in the request"};
        return res.status(400).send(error);
    }

    if (message.author.length === 0) {
        message.author = 'Anonymous';
    }
    fileDb.addItem(message);
    res.send(message);
});

module.exports = router