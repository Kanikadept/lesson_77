import React from 'react';
import './App.css';
import Chat from "./containers/Chat/Chat";

const App = () => (
    <div className="App">
        <Chat />
    </div>
);

export default App;
