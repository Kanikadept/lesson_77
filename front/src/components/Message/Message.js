import React from 'react';
import './Message.css';

const Message = ({text, author, time, image}) => {
    return (
        <div className="message">
            <p><strong>Author: </strong>{author}</p>
            <p><strong>Message: </strong>{text}</p>
            <p><strong>message time: </strong>{time}</p>
            {image && (
                <div className="message-image">
                    <img src={'http://localhost:8000/uploads/' + image} alt=""/>
                </div>
            )}
        </div>
    );
};

export default Message;