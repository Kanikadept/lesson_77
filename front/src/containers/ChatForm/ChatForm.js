import React, {useState} from 'react';
import './ChatForm.css';
import {useDispatch} from "react-redux";
import {createMessage} from "../../store/actions/messageActions";
import FileInput from './FileInput/FileInput'

const ChatForm = () => {

    const dispatch = useDispatch();

    const [inputVal, setInputVal] = useState({
        author: '',
        message: '',
        image: '',
    });

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setInputVal(prevSate => ({
            ...prevSate,
                [name]: file
        }));
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(inputVal).forEach(key => {
            formData.append(key, inputVal[key]);
        });
        dispatch(createMessage(formData));
        setInputVal({
            author: '',
            message: '',
            image: '',
        })
    }

    const handleChange = (event) => {
        const {name, value} = event.target;
        const inputValCopy = {...inputVal};

        inputValCopy[name] = value;

        setInputVal(inputValCopy);
    }

    return (
        <form className="chat-form" onSubmit={handleSubmit}>
            <input type="text" name="author"
                   placeholder="enter ur name:"
                   value={inputVal.author}
                   onChange={handleChange}/>
            <input type="text" name="message"
                   placeholder="here ur message:"
                   value={inputVal.message}
                   onChange={handleChange}
                   required/>
            <FileInput
                name="image"
                label="Image"
                onChange={fileChangeHandler}/>

            <button>Send</button>
        </form>
    );
};

export default ChatForm;