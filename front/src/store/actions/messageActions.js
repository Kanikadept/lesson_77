import axiosApi from "../../axiosApi";
import {NotificationManager} from "react-notifications";

export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, messages});
export const fetchMessages = () => {
    return async dispatch => {
        try {
            const messagesResponse = await axiosApi.get('/messages');
            dispatch(fetchMessagesSuccess(messagesResponse.data));
            dispatch(setLastTimeMessagesSuccess(messagesResponse.data[0].datetime));
        } catch (e) {
            NotificationManager.error("Could not fetch messages!");
        }

    }
}

export const createMessage = message => {
    return async dispatch => {
        try {
            await axiosApi.post('/messages', message);
        } catch (e) {
            NotificationManager.error("Message is empty !");
        }

    }
}

export const SET_LAST_TIME_MESSAGES = 'SET_LAST_TIME_MESSAGES';
export const setLastTimeMessagesSuccess = datetime => ({type: SET_LAST_TIME_MESSAGES, datetime});

export const FETCH_LAST_TIME_MESSAGES_SUCCESS = 'FETCH_LAST_TIME_MESSAGES_SUCCESS';
export const fetchLastTimeMessagesSuccess = messages => ({type: FETCH_LAST_TIME_MESSAGES_SUCCESS, messages});
export const fetchLastTimeMessages = (datetime) => {
    return async dispatch => {
        try {
            const messagesResponse = await axiosApi.get(`/messages?datetime=${datetime}`);
            if (messagesResponse.data.length !== 0) {
                dispatch(fetchLastTimeMessagesSuccess(messagesResponse.data));
                dispatch(setLastTimeMessagesSuccess(messagesResponse.data[0].datetime));
            }
        } catch (e) {
            NotificationManager.error("Could not fetch new messages !");
        }


    }
}