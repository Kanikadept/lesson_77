import {
    CREATE_MESSAGE_SUCCESS,
    FETCH_LAST_TIME_MESSAGES_SUCCESS,
    FETCH_MESSAGES_SUCCESS,
    SET_LAST_TIME_MESSAGES
} from "../actions/messageActions";

const initialState = {
    messages: [],
    lastMessageTime: null,
}
const messageReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_LAST_TIME_MESSAGES:
            return {...state, lastMessageTime: action.datetime};
        case FETCH_LAST_TIME_MESSAGES_SUCCESS:
            return {...state, messages: [...action.messages, ...state.messages]};
        case FETCH_MESSAGES_SUCCESS:
            return {...state, messages: action.messages};
        default:
            return state;
    }
};

export default messageReducer;